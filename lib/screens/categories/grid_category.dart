import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/category.dart';
import '../../models/product.dart';

class GridCategory extends StatefulWidget {
  @override
  _StateGridCategory createState() => _StateGridCategory();
}

class _StateGridCategory extends State<GridCategory> {
  Future<List<Category>> getAllCategory() async {
    List<Category> _categories = [];
    final categories =
        Provider.of<CategoryModel>(context, listen: false).categories;
    var listCategories = categories.where((item) => item.parent == 0).toList();
    for (var category in listCategories) {
      var children = categories.where((o) => o.parent == category.id).toList();
      if (children.isNotEmpty) {
        _categories = [..._categories, ...children];
      } else {
        _categories = [..._categories, category];
      }
    }
    return _categories;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Category>>(
      future: getAllCategory(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return Container();
        return Wrap(
          alignment: WrapAlignment.spaceAround,
          children: <Widget>[
            for (int i = 0; i < snapshot.data.length; i++)
              if (kGridIconsCategories[snapshot.data[i].id] != null)
                GestureDetector(
                  child: Container(
                    width: MediaQuery.of(context).size.width /
                            kAdvanceConfig['GridCount'] -
                        20 * kAdvanceConfig['GridCount'],
                    margin: EdgeInsets.all(20.0),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Image.asset(
                              kGridIconsCategories[snapshot.data[i].id],
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          SizedBox(height: 8.0),
                          Text(
                            snapshot.data[i].name,
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                    Product.showList(
                        context: context,
                        cateId: snapshot.data[i].id,
                        cateName: snapshot.data[i].name);
                  },
                )
          ],
        );
      },
    );
  }
}
