import 'package:flutter/material.dart';

import '../checkout/index.dart';
import 'my_cart.dart';

class CartScreen extends StatefulWidget {
  final bool isModal;
  final bool isBuyNow;

  CartScreen({this.isModal, this.isBuyNow = false});

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: PageView(
        controller: pageController,
        children: <Widget>[
          MyCart(
            controller: pageController,
            isBuyNow: widget.isBuyNow,
            isModal: widget.isModal,
          ),
          Checkout(controller: pageController, isModal: widget.isModal),
        ],
      ),
    );
  }
}
