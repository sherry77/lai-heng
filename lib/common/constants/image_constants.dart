class ImageConstants {
  static const String logo = 'assets/images/logo_png.png';
  static const String emptySearch = 'assets/images/empty_search.png';

  static const String countryGB = 'assets/images/country/gb.png';
  static const String countryVN = 'assets/images/country/vn.png';
  static const String countryJA = 'assets/images/country/ja.png';
  static const String countryZH = 'assets/images/country/zh.png';
  static const String countryES = 'assets/images/country/es.png';
  static const String countryAR = 'assets/images/country/ar.png';

  static const String orderCompleted = 'assets/images/fogg-order-completed.png';
}
