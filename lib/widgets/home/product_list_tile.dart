import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../models/product.dart';
import '../../models/recent_product.dart';
import '../../route.dart';
import '../../screens/detail/index.dart';

class ProductListTitle extends StatelessWidget {
  final List<Product> products;

  ProductListTitle(this.products);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Container(
          width: constraints.maxWidth,
          height: constraints.maxWidth * (kLayoutWeb ? 0.4 : 0.6),
          child: PageView(
            children: <Widget>[
              for (var i = 0; i < products.length; i = i + 3)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    products[i] != null
                        ? Expanded(
                            child: ProductItemTileView(
                                products: products, index: i),
                          )
                        : Expanded(
                            child: Container(),
                          ),
                    i + 1 < products.length
                        ? Expanded(
                            child: ProductItemTileView(
                                products: products, index: i + 1),
                          )
                        : Expanded(
                            child: Container(),
                          ),
                    i + 2 < products.length
                        ? Expanded(
                            child: ProductItemTileView(
                                products: products, index: i + 2),
                          )
                        : Expanded(
                            child: Container(),
                          ),
                  ],
                )
            ],
          ),
        );
      },
    );
  }
}

class ProductItemTileView extends StatelessWidget {
  final List<Product> products;
  final int index;

  ProductItemTileView({this.products, this.index});

  void onTapProduct(context) {}

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (products[index].imageFeature == '') return;
        Provider.of<RecentModel>(context, listen: false)
            .addRecentProduct(products[index]);
        print('item id: ${products[index].id}');
        //Load update product detail screen for FluxBuilder
        eventBus.fire(-1);

        Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) => RouteAwareWidget(
              'listTile',
              child: Detail(product: products[index]),
            ),
            fullscreenDialog: kLayoutWeb,
          ),
        );
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        constraints: BoxConstraints(
          maxWidth: kLayoutWeb
              ? MediaQuery.of(context).size.width * 0.6
              : MediaQuery.of(context).size.width,
        ),
        child: FittedBox(
          fit: BoxFit.fitWidth,
          child: Container(
            width: 400,
            child: ListTile(
              leading: Container(
                width: 50,
                height: 100,
                padding: const EdgeInsets.symmetric(vertical: 4.0),
                alignment: Alignment.center,
                child: Tools.image(
                  url: products[index].imageFeature,
                  width: 50,
                  size: kSize.medium,
                  isVideo: Videos.getVideoLink(products[index].videoUrl) == null
                      ? false
                      : true,
                ),
              ),
              title: Text(products[index].name,
                  maxLines: 2, style: TextStyle(fontSize: 15.0)),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  Tools.getCurrecyFormatted(products[index].price),
                  style: TextStyle(fontSize: 11, color: Colors.black45),
                ),
              ),
              dense: false,
            ),
          ),
        ),
      ),
    );
  }
}
