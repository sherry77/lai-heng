/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.bestweb.laiheng;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.bestweb.laiheng";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 34;
  public static final String VERSION_NAME = "1.6.6";
}
