/**
 * Automatically generated file. DO NOT MODIFY
 */
package name.avioli.unilinks;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "name.avioli.unilinks";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
