/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.aboutyou.dart_packages.sign_in_with_apple;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.aboutyou.dart_packages.sign_in_with_apple";
  public static final String BUILD_TYPE = "profile";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
