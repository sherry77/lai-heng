/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.flutter.plugins.video_player_web;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "io.flutter.plugins.video_player_web";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
